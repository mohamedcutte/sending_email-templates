# Node.JS Email Templates

Sending templated emails with Node.JS.

**NOTE**: `email-templates` version 3 contains breaking changes that are incompatible with this exact code. I'll add a branch to this repo that contains working code for `email-templates` version 3, otherwise you can install `email-templates@2` or clone this repo and install the packages from `package.json` and it should work fine.

## Setup from scratch

    $ mkdir nodejs-template-emails
    $ cd nodejs-template-emails
    $ npm init
    $ npm install --save nodemailer email-templates handlebars bluebird

## Setup from repo

    $ git clone git@gitlab.com:mohamedcutte/sending_email-templates.git
    $ cd nodejs-template-emails
    $ npm install


